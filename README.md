The second commit should crash gitlab's diff viewer.

See https://gitlab.com/gitlab-org/gitlab-ce/issues/32246
